# pwrnd - A very simple password generator

The generated passwords will only contain characters
from the `a` - `z`, `A` - `Z` and `0` - `9` ranges.
The characters `B8G6I1l0OQDS5Z2` are omitted because
they are not always easy to distinguish.

## Build

You need a [Go](https://golang.org) build environment.

```
go get -u -v gitlab.com/sascha.l.teichmann/pwrnd
```

Place the resulting `pwrnd` binary into your `PATH`

## Usage

```
pwrnd 8 
```

Prints a random password of length eight followed by a new line on `STDOUT`.
The length of the password defaults to 12.  
For more options see `pwrnd --help`.

## License

This is Free Software covered by the terms of the [MIT license](LICENSE.txt).  
Copyright 2020 by [Sascha L. Teichmann](mailto:sascha.teichmann@intevation.de).
