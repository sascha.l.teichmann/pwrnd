// This is Free Software covered by the terms of the MIT license.
// See LICENSE.txt for details.
//
// SPDX-FileCopyrightText: 2020 Sascha L. Teichmann <sascha.teichmann@intevation.de>
// SPDX-License-Identifier: MIT

package main

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/big"
	"os"
	"strconv"
)

var (
	digits  = []byte("0123456789")
	lower   = []byte("abcdefghijklmnopqrstuvwxyz")
	upper   = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	symbols = []byte("!@#$%^&*")
	// not always easy to distinguish
	ambiguous = []byte("B8G6I1l0OQDS5Z2!$")
)

type generator struct {
	io.Reader
	table []byte
}

func (g *generator) append(chars []byte) {
	g.table = append(g.table, chars...)
}

func remove(a, chars []byte) []byte {
	b := a[:0]
	for _, x := range a {
		if bytes.IndexByte(chars, x) == -1 {
			b = append(b, x)
		}
	}
	return b
}

func (g *generator) remove(chars []byte) {
	g.table = remove(g.table, chars)
}

func (g *generator) printPossibilities(out io.Writer, n uint, reduced bool) {
	all := uint64(len(g.table))
	passwords := uint64(1)
	if reduced {
		fmt.Fprintln(out, "You are using extra conditions (e.g. forcing a symbol).")
		fmt.Fprintln(out, "Therefore the possibilties are lower than printed here.")
	}
	fmt.Fprintln(out, "|=  characters |= possibilties       |")
	exp := math.Log2(float64(all))
	for i := uint(1); i <= n; i++ {
		var s string
		if i <= 5 {
			passwords *= all
			s = strconv.FormatUint(passwords, 10)
		} else {
			s = "..."
		}
		log2 := fmt.Sprintf("2^%4.2f", exp*float64(i))
		fmt.Fprintf(out, "| %12d |%10s ~ %7s |\n", i, s, log2)
	}
}

func (g *generator) Read(p []byte) (int, error) {
	max := big.NewInt(int64(len(g.table)))
	for i := range p {
		v, err := rand.Int(g.Reader, max)
		if err != nil {
			return i, err
		}
		p[i] = g.table[v.Int64()]
	}
	return len(p), nil
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(),
		"Usage: %s [OPTION]... [password length] [number passwords]\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	var (
		noNL, pos, syms, noAmbi, forceSym bool
		nChars                            uint
	)

	flag.UintVar(&nChars, "chars", 12, "number of chars in password")
	flag.UintVar(&nChars, "c", 12, "number of chars in password (shorthand)")
	flag.BoolVar(&noNL, "no-newline", false, "print no new line behind password")
	flag.BoolVar(&noNL, "N", false, "print no new line behind password (shorthand)")
	flag.BoolVar(&pos, "possibilties", false, "print number of possible passwords")
	flag.BoolVar(&pos, "p", false, "print number of possible passwords (shorthand)")
	flag.BoolVar(&syms, "symbols", false, "use specials chars")
	flag.BoolVar(&syms, "s", false, "use specials chars (shorthand)")
	flag.BoolVar(&noAmbi, "no-ambiguous", false, "use no ambiguous chars")
	flag.BoolVar(&noAmbi, "A", false, "use no ambiguous chars (shorthand)")
	flag.BoolVar(&forceSym, "force-symbol", false, "force at least one symbol in passwords")
	flag.BoolVar(&forceSym, "S", false, "force at least one symbol in passwords (shorthand)")

	flag.Usage = usage

	flag.Parse()

	var used bool

	flag.Visit(func(f *flag.Flag) {
		if f.Name == "c" || f.Name == "chars" {
			used = true
		}
	})

	nPWs := uint(1)
	argc := 0
args:
	for _, arg := range flag.Args() {
		v, err := strconv.ParseUint(arg, 10, 32)
		if err != nil {
			log.Printf("warn: unable to parse '%s' as positive integer.\n", arg)
			continue
		}
		switch argc {
		case 0:
			if used && uint(v) != nChars {
				log.Fatalf("error: ambiguous numbers of chars: %d != %d\n", nChars, v)
			}
			nChars = uint(v)
			argc++
		case 1:
			nPWs = uint(v)
			break args
		}
	}

	if forceSym {
		syms = true
	}

	if forceSym && nChars == 0 {
		log.Fatalln("error: For forcing a symbol at least one character is needed.")
	}

	r := generator{Reader: rand.Reader}

	r.append(digits)
	r.append(lower)
	r.append(upper)

	if syms {
		r.append(symbols)
	}
	if noAmbi {
		r.remove(ambiguous)
	}

	out := bufio.NewWriter(os.Stdout)

	if pos {
		r.printPossibilities(out, nChars, forceSym)
	}

	var findSym string
	if forceSym {
		syms := append(make([]byte, 0, len(symbols)), symbols...)
		if noAmbi {
			syms = remove(syms, ambiguous)
		}
		findSym = string(syms)
	}

	var sep string
	if noNL {
		sep = " "
	} else {
		sep = "\n"
	}

	password := make([]byte, nChars)

	for i := uint(0); i < nPWs; i++ {
		for {
			if _, err := io.ReadFull(&r, password); err != nil {
				log.Fatalf("error: %v\n", err)
			}
			if forceSym && !bytes.ContainsAny(password, findSym) {
				continue
			}
			break
		}
		if i > 0 {
			fmt.Fprint(out, sep)
		}
		fmt.Fprint(out, string(password))
	}

	if !noNL {
		fmt.Fprintln(out)
	}

	if err := out.Flush(); err != nil {
		log.Printf("error: %v\n", err)
	}
}
